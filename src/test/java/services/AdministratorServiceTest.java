
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Administrator;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class AdministratorServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private AdministratorService administratorService;

	@Autowired
	ApplyService					applyService;

	@Autowired
	CourseService					courseService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated must be able to: Edit his or her personal data.
	// Comprobamos que se puede editar un administrator correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos o incorrectos.
	// Test positivo y 2 tests negativos
	@Test
	public void editAdministrator() {

		List<Administrator> administrators = (List<Administrator>) administratorService.findAll();
		final Object testingData[][] = {
			{
				"admin", administrators.get(0).getId(), null
			}, {
				"admin", null, NullPointerException.class
			}, {
				"admin", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template2(String username, Integer administratorId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			System.out.println("#editAdministrator");
			Administrator administrator = administratorService.findOne(administratorId);
			administrator.setName("Name Modified");
			administrator.setEmail("EmailModified@GMAIL.COM");
			Administrator administratorSaved = administratorService.saveAndFlush(administrator);
			Assert.isTrue(administratorSaved != null && administratorSaved.getId() != 0);

			System.out.println(administratorSaved.getName() + "-" + administratorSaved.getEmail());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an administrator must be able to: Display a dashboard
	 * El test comprobar� que todas las queries utilizadas para mostrar los datos del dashboard
	 * devuelven valores correctos
	 *
	 * Test positivo obteniendo los datos de las queries
	 * Test negativo sin estar logado como Administrador
	 * No se hace segundo test negativo ya que las queries de dashboard no reciben par�metros y no se puede probar nada mas
	 */
	@Test
	public void compruebaDashboard() {

		final Object testingData[][] = {
			{
				"admin", null
			}, {
				"academy1", IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
	protected void template(String username, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Collection<Object> minMaxAvgAndStadevOfAppliesPerCourse = applyService.minMaxAvgAndStadevOfAppliesPerCourse();
			Assert.notNull(minMaxAvgAndStadevOfAppliesPerCourse);
			Collection<Object> minMaxAvgAndStadevOfCoursesPerAcademy = courseService.minMaxAvgAndStadevOfCoursesPerAcademy();
			Assert.notNull(minMaxAvgAndStadevOfCoursesPerAcademy);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}
}
