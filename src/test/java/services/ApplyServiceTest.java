
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Academy;
import domain.Apply;
import domain.Course;
import domain.Dancer;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ApplyServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private DancerService dancerService;

	@Autowired
	private AcademyService	academyService;

	@Autowired
	private ApplyService	applyService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated as a dancer must be able to: List his or her applications.
	// Comprobamos que los applications se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativos
	@Test
	public void listApplicationsByDancer() {

		List<Dancer> dancers = (List<Dancer>) dancerService.findAll();
		Object testingData[][] = {
			{
				"dancer1", dancers.get(0).getId(), null
			}, {
				"dancer1", null, NullPointerException.class
			}, {
				"dancer1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	protected void template(String username, Integer dancerId, Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Dancer dancer = dancerService.findOne(dancerId);
			System.out.println("#listApplicationsByDancer");
			Assert.isTrue(dancer.getApplies() != null && !dancer.getApplies().isEmpty());
			for (Apply o : dancer.getApplies())
				System.out.println(o.getStatus() + "-" + o.getCourse().getTitle());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/**
	 * An actor who is authenticated as an academy must be able to: Accept or reject a dancer's application regarding the courses that they manage.
	 *
	 * Test positivo aceptando y rechazando correctamente una solicitud
	 * Test negativo sin estar logado como academy
	 * Test negativo aportando un id de apply incorrecto
	 */

	@Test
	public void academyAcceptRejectApply() {

		/*
		 * Se elige un academy que tenga un course con solicitudes no aceptadas
		 * para poder aceptarlas y rechazarlas posteriormente
		 */

		List<Academy> academies = (List<Academy>) this.academyService.findAll();
		Assert.isTrue(academies != null && !academies.isEmpty());
		Academy academy = null;
		Apply apply = null;
		for (Academy c : academies) {
			if (c.getCourses() != null && !c.getCourses().isEmpty()) {
				for (Course g : c.getCourses()) {
					if (g.getApplies() != null && !g.getApplies().isEmpty()) {
						for (Apply a : g.getApplies()) {
							if (!a.getStatus().equals("accepted")) {
								apply = a;
								academy = c;
								break;
							}
						}
					}
				}
			}
		}

		Assert.isTrue(academy != null && apply != null, "No se han encontrado academy y apply para realizar la prueba");

		/*
		 * Usamos el id del academy como id de apply incorrecto
		 */
		Integer idApply = academy.getId();

		Object testingData[][] = {
			{
				academy.getUserAccount().getUsername(), apply.getId(), null
			}, {
				"admin", null, NullPointerException.class
			}, {
				academy.getUserAccount().getUsername(), idApply, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	protected void template2(String username, Integer applyId, Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Apply applyAceptada = this.applyService.setApplyAsAccepted(applyId);
			Assert.notNull(applyAceptada);
			Apply applyRechazada = this.applyService.setApplyAsRejected(applyId);
			Assert.notNull(applyRechazada);

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

}
