
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Dancer;
import forms.DancerForm;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class DancerServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private DancerService dancerService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated must be able to: Edit his or her personal data.
	// Comprobamos que se puede editar un dancer correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos o incorrectos.
	// Test positivo y 2 tests negativos

	protected void template2(String username, Integer dancerId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			System.out.println("#editDancer");
			Dancer dancer = this.dancerService.findOne(dancerId);
			dancer.setName("Name Modified");
			dancer.setEmail("EmailModified@GMAIL.COM");
			Dancer dancerSaved = this.dancerService.saveAndFlush(dancer);
			Assert.isTrue(dancerSaved != null && dancerSaved.getId() != 0);

			System.out.println(dancerSaved.getName() + "-" + dancerSaved.getEmail());

			this.unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is not authenticated must be able to: Register to the system as a dancer or a academy.
	 *
	 * En este caso de uso se llevara a cabo el registro de un dancer en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerDancer(String username, String name, String surname, String email, String phone, String postalAddress, String newUsername, String password, String secondPassword, Boolean checkBox, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);

			// Inicializamos los atributos para la creaci�n
			DancerForm actor = new DancerForm();

			actor.setName(name);
			actor.setSurname(surname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPostalAddress(postalAddress);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Reconsturimos
			final Dancer dancer = this.dancerService.reconstruct(actor);

			//Comprobamos atributos
			this.dancerService.comprobacion(dancer);

			//Guardamos
			this.dancerService.saveForm(dancer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void editDancer() {

		List<Dancer> dancers = (List<Dancer>) this.dancerService.findAll();
		final Object testingData[][] = {
			{
				"dancer1", dancers.get(0).getId(), null
			}, {
				"dancer1", null, NullPointerException.class
			}, {
				"dancer1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void registerDancerDriver() {

		final Object testingData[][] = {
			// Creaci�n de academy como autentificado (1) -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "username1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy como autentificado (2) -> false
			{
				"dancer1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "username2", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy como autentificado (3) -> false
			{
				"academy1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy con postalAddress incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "56118916511", "username4", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy sin aceptar t�rminos -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "username5", "password1", "password1", false, IllegalArgumentException.class
			},
			// Creaci�n de academy con usuario no �nico -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "academy1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy con contrase�as no coincidentes -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "username7", "password1", "password2", true, IllegalArgumentException.class
			},
			// Creaci�n de academy con tel�fono incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "41010", "username10", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy con todo correcto -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "username8", "password1", "password1", true, null
			},
			// Creaci�n de academy con codigopostal vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "", "username9", "password1", "password1", true, null
			},
			// Creaci�n de academy con telefono vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "", "41010", "username13", "password1", "password1", true, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerDancer((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (Boolean) testingData[i][9], (Class<?>) testingData[i][10]);
	}
}
