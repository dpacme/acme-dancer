
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Academy;
import domain.Course;
import forms.AcademyForm;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class AcademyServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private AcademyService academyService;
	@Autowired
	private CourseService courseService;

	// Templates --------------------------------------------------------------

	// An actor who is not authenticated/authenticated must be able to: Browse the catalogue of courses and navigate to the academies that offer them.
	// Comprobamos que los cursos se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo
	protected void template3(Integer academyId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			Academy academy = academyService.findOne(academyId);
			System.out.println("#listAcademies");
			Assert.isTrue(academy != null);
			System.out.println(academy.getName());

		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}
	
	// An actor who is authenticated must be able to: Edit his or her personal data.
	// Comprobamos que se puede editar un academy correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos o incorrectos.
	// Test positivo y 2 tests negativos

	protected void template2(String username, Integer academyId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			System.out.println("#editAcademy");
			Academy academy = academyService.findOne(academyId);
			academy.setName("Name Modified");
			academy.setComercialName("Comercial modified");
			Academy academySaved = academyService.saveAndFlush(academy);
			Assert.isTrue(academySaved != null && academySaved.getId() != 0);

			System.out.println(academySaved.getName() + "-" + academySaved.getComercialName());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * An actor who is not authenticated must be able to: Register to the system as a dancer or a academy.
	 *
	 * En este caso de uso se llevara a cabo el registro de una academy en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerAcademy(String username, String name, String surname, String email, String phone, String postalAddress, String comercialName, String newUsername, String password, String secondPassword, Boolean checkBox, Class<?> expected) {

		Class<?> caught = null;

		try {

			authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);

			// Inicializamos los atributos para la creaci�n
			AcademyForm actor = new AcademyForm();

			actor.setName(name);
			actor.setSurname(surname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPostalAddress(postalAddress);
			actor.setComercialName(comercialName);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Reconsturimos
			final Academy academy = academyService.reconstruct(actor);

			//Comprobamos atributos
			academyService.comprobacion(academy);

			//Guardamos
			academyService.saveForm(academy);

			unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		checkExceptions(expected, caught);
	}

	/*
	 * Browse the catalogue of academies and navigate to the courses that they offer.
	 *
	 * Se prueba el listado con varios actores logados y la obtenci�n de los cursos de una academ�a.
	 */
	public void catalogueAcademies(String username, Class<?> expected) {

		Class<?> caught = null;

		try {
			if (username != null) {
				authenticate(username);
			}

			List<Academy> academies = (List<Academy>) academyService.findAll();
			Academy academy = academyService.create();
			System.out.println("###catalogueAcademies##");
			System.out.println("\nAcademies:\n");
			for (Academy a : academies) {
				System.out.println("Name=" + a.getName() + "\n");
			}

			if (academies != null && !academies.isEmpty()) {
				Academy aca = academyService.findOne(academies.get(0).getId());
				Assert.notNull(academy);
				Collection<Course> courses = aca.getCourses();

				System.out.println("Courses of academy:" + aca.getName() + "\n");
				for (Course course : courses) {
					System.out.println("Tittle=" + course.getTitle() + "\n");
				}
			}
			if (username != null) {
				unauthenticate();
			}
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void editAcademy() {

		List<Academy> academies = (List<Academy>) academyService.findAll();
		final Object testingData[][] = {
			{
				"academy1", academies.get(0).getId(), null
			}, {
				"academy1", null, NullPointerException.class
			}, {
				"academy1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++) {
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	@Test
	public void registerAcademyDriver() {

		final Object testingData[][] = {
			// Creaci�n de academy como autentificado (1) -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "Comercial name", "username1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy como autentificado (2) -> false
			{
				"dancer1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "Comercial name", "username2", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy como autentificado (3) -> false
			{
				"academy1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "Comercial name", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy con postalAddress incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "56118916511", "Comercial name", "username4", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy sin aceptar t�rminos -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "Comercial name", "username5", "password1", "password1", false, IllegalArgumentException.class
			},
			// Creaci�n de academy con usuario no �nico -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "Comercial name", "academy1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy con contrase�as no coincidentes -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "Comercial name", "username7", "password1", "password2", true, IllegalArgumentException.class
			},
			// Creaci�n de academy con tel�fono incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "41010", "Comercial name", "username10", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de academy con todo correcto -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "Comercial name", "username8", "password1", "password1", true, null
			},
			// Creaci�n de academy con codigopostal vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "", "Comercial name", "username9", "password1", "password1", true, null
			},
			// Creaci�n de academy con telefono vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "", "41010", "Comercial name", "username13", "password1", "password1", true, null
			}
		};
		for (int i = 0; i < testingData.length; i++) {
			registerAcademy((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (String) testingData[i][9], (Boolean) testingData[i][10], (Class<?>) testingData[i][11]);
		}
	}

	@Test
	public void catalogueAcademyDriver() {

		final Object testingData[][] = {
			{
				"admin", null
			}, {
				"dancer1", null
			}, {
				"academy1", null
			},//Test negativo, usuario inexistente
			{
				"manager1", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++) {
			catalogueAcademies((String) testingData[i][0], (Class<?>) testingData[i][1]);
		}
	}
	

	@Test
	public void listAcademyFromCourses() {
		
		Collection<Course> courses = courseService.findAll();
		Course course = null;
		for(Course o:courses) {
			if(o.getAcademy() != null) {
				course = o;
				break;
			}
		}
		Academy academy = course.getAcademy();

		Object testingData[][] = {
			{
				academy.getId(), null
			}, {
				null, NullPointerException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template3((Integer) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
