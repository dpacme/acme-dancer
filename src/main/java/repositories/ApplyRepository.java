
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Apply;

@Repository
public interface ApplyRepository extends JpaRepository<Apply, Integer> {

	@Query("select min(c.applies.size), max(c.applies.size), avg(c.applies.size), stddev(c.applies.size) from Course c")
	Collection<Object> minMaxAvgAndStadevOfAppliesPerCourse();

}
