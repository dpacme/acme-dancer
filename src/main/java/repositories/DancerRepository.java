
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Dancer;
import security.UserAccount;

@Repository
public interface DancerRepository extends JpaRepository<Dancer, Integer> {

	@Query("select a from Dancer a where a.userAccount = ?1")
	Dancer findByUserAccount(UserAccount userAccount);
}
