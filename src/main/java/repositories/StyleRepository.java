
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import domain.Style;

@Repository
public interface StyleRepository extends JpaRepository<Style, Integer> {

}
