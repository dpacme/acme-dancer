
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Academy;
import security.UserAccount;

@Repository
public interface AcademyRepository extends JpaRepository<Academy, Integer> {

	@Query("select a from Academy a where a.userAccount = ?1")
	Academy findByUserAccount(UserAccount userAccount);
}
