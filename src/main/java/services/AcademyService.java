
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Academy;
import domain.Actor;
import domain.Course;
import forms.AcademyForm;
import repositories.AcademyRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class AcademyService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private AcademyRepository academyRepository;


	// Constructor methods --------------------------------------------------------------
	public AcademyService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	@Autowired
	private ActorService actorService;

	// Simple CRUD methods --------------------------------------------------------------

	public Academy findOne(final int academyId) {
		Assert.isTrue(academyId != 0);
		Academy result;

		result = this.academyRepository.findOne(academyId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Academy> findAll() {
		Collection<Academy> result;

		result = this.academyRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Academy academy) {
		Assert.notNull(academy);
		this.academyRepository.save(academy);
	}

	public Academy create() {
		final Academy academy = new Academy();
		return academy;
	}

	public void delete(final Academy academy) {
		Assert.notNull(academy);
		this.academyRepository.delete(academy);
	}

	public Academy saveAndFlush(final Academy academy) {
		Assert.notNull(academy);

		return this.academyRepository.saveAndFlush(academy);
	}

	// Other bussines methods -----------------------------------------------------

	public Academy reconstruct(AcademyForm a) {
		Academy academy = new Academy();
		Collection<Course> courses = new ArrayList<Course>();

		UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		academy.setId(0);
		academy.setVersion(0);
		academy.setName(a.getName());
		academy.setSurname(a.getSurname());
		academy.setEmail(a.getEmail());
		academy.setPhone(a.getPhone());
		academy.setPostalAddress(a.getPostalAddress());
		academy.setComercialName(a.getComercialName());

		academy.setUserAccount(account);

		academy.setCourses(courses);

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return academy;
	}

	public List<String> comprobacionEditarListErrores(Academy academy) {
		List<String> listaErrores = new ArrayList<String>();
		if (academy.getPostalAddress() != "" && !academy.getPostalAddress().matches("^(\\d{5}$)"))
			listaErrores.add("postal");
		if (academy.getPhone() != "" && !academy.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
			listaErrores.add("phone");

		return listaErrores;
	}

	public void saveForm(Academy academy) {

		Assert.notNull(academy);
		String password;
		Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		Collection<Authority> auths = new ArrayList<>();
		Authority auth = new Authority();
		auth.setAuthority("ACADEMY");
		auths.add(auth);

		password = academy.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		academy.getUserAccount().setPassword(password);

		academy.getUserAccount().setAuthorities(auths);

		academy = this.academyRepository.saveAndFlush(academy);
	}

	public Academy findByPrincipal() {
		Academy result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.academyRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public void comprobacion(final Academy academy) {
		if (academy.getPostalAddress() != "")
			Assert.isTrue(academy.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (academy.getPhone() != "")
			Assert.isTrue(academy.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
		for (final Actor actor : this.actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(academy.getUserAccount().getUsername()));
	}

	public void checkIfAcademy() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.ACADEMY))
				res = true;
		Assert.isTrue(res);
	}

}
