
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Academy;
import domain.Administrator;
import domain.Apply;
import repositories.ApplyRepository;

@Service
@Transactional
public class ApplyService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ApplyRepository applyRepository;


	// Constructor methods --------------------------------------------------------------
	public ApplyService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private AcademyService academyService;

	@Autowired
	private AdministratorService	administratorService;


	// Simple CRUD methods --------------------------------------------------------------

	public Apply findOne(int applyId) {
		Assert.isTrue(applyId != 0);
		Apply result;

		result = applyRepository.findOne(applyId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Apply> findAll() {
		Collection<Apply> result;

		result = applyRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(Apply apply) {
		Assert.notNull(apply);
		applyRepository.save(apply);
	}

	public Apply create() {
		Apply apply = new Apply();
		return apply;
	}

	public void delete(Apply apply) {
		Assert.notNull(apply);
		applyRepository.delete(apply);
	}

	public Apply saveAndFlush(Apply apply) {
		Assert.notNull(apply);

		return applyRepository.saveAndFlush(apply);
	}

	// Other bussines methods -----------------------------------------------------

	public Apply setApplyAsPending(int applyID) {
		Academy academy = academyService.findByPrincipal();
		Assert.notNull(academy);

		Apply apply = findOne(applyID);
		Assert.notNull(apply);

		//Se comprueba que el apply pertenece a una oferta de la compa��a logada
		Assert.isTrue(apply.getCourse().getAcademy().getId() == academy.getId());

		apply.setStatus("pending");
		return saveAndFlush(apply);
	}

	public Apply setApplyAsAccepted(int applyID) {
		Academy academy = academyService.findByPrincipal();
		Assert.notNull(academy);

		Apply apply = findOne(applyID);
		Assert.notNull(apply);

		//Se comprueba que el apply pertenece a una oferta de la compa��a logada
		Assert.isTrue(apply.getCourse().getAcademy().getId() == academy.getId());

		apply.setStatus("accepted");
		return saveAndFlush(apply);
	}

	public Apply setApplyAsRejected(int applyID) {
		Academy academy = academyService.findByPrincipal();
		Assert.notNull(academy);

		Apply apply = findOne(applyID);
		Assert.notNull(apply);

		//Se comprueba que el apply pertenece a una oferta de la compa��a logada
		Assert.isTrue(apply.getCourse().getAcademy().getId() == academy.getId());

		apply.setStatus("rejected");
		return saveAndFlush(apply);
	}

	//QUERY - The minimum, the average, the standard deviation, and the maximum num-ber of applications per course.
	public Collection<Object> minMaxAvgAndStadevOfAppliesPerCourse() {
		Administrator admin = administratorService.findByPrincipal();
		Assert.notNull(admin);
		return this.applyRepository.minMaxAvgAndStadevOfAppliesPerCourse();
	}

}
