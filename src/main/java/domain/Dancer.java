
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Dancer extends Actor {

	// Relationships -------------------------------------------

	private Collection<Apply> applies;


	@Valid
	@OneToMany(mappedBy = "dancer")
	public Collection<Apply> getApplies() {
		return applies;
	}

	public void setApplies(final Collection<Apply> applies) {
		this.applies = applies;
	}

}
