
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Apply extends DomainEntity {

	private Date	momment;
	private String	status;


	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMomment() {
		return this.momment;
	}

	public void setMomment(final Date momment) {
		this.momment = momment;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Pattern(regexp = "^(pending|accepted|rejected)$")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}


	// Relationships -------------------------------------------

	private Course	course;
	private Dancer	dancer;


	@Valid
	@ManyToOne(optional = false)
	public Course getCourse() {
		return this.course;
	}

	public void setCourse(final Course course) {
		this.course = course;
	}

	@Valid
	@ManyToOne(optional = false)
	public Dancer getDancer() {
		return this.dancer;
	}

	public void setDancer(final Dancer dancer) {
		this.dancer = dancer;
	}

}
