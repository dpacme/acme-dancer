
package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Academy;
import domain.Course;
import domain.Style;
import services.AcademyService;
import services.CourseService;
import services.StyleService;

@Controller
@RequestMapping("/course")
public class CourseController extends AbstractController {

	// Services
	@Autowired
	private CourseService	courseService;

	@Autowired
	private AcademyService	academyService;

	@Autowired
	private StyleService	styleService;


	// Constructors -----------------------------------------------------------

	public CourseController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/coursesByAcademy", method = RequestMethod.GET)
	public ModelAndView coursesByAcademy(@RequestParam final int academyId) {
		ModelAndView result;
		final Academy academy = this.academyService.findOne(academyId);

		final Collection<Course> courses = academy.getCourses();

		result = new ModelAndView("course/list");
		result.addObject("courses", courses);
		result.addObject("academyId", academyId);
		result.addObject("requestURI", "course/coursesByAcademy.do");

		return result;
	}

	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay(@RequestParam final int courseId) {
		ModelAndView result;
		final Course course = this.courseService.findOne(courseId);

		result = new ModelAndView("course/showDisplay");
		result.addObject("course", course);
		result.addObject("starDate", course.getStartDate());
		result.addObject("endDate", course.getEndDate());
		result.addObject("time", course.getTime());
		result.addObject("requestURI", "course/showDisplay.do");

		return result;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;

		final Collection<Course> courses = this.courseService.findAll();

		result = new ModelAndView("course/list");
		result.addObject("courses", courses);
		result.addObject("requestURI", "course/all.do");

		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, params = "search")
	public ModelAndView searchPost(final String keyword, final Integer academyId, final Integer styleId) {
		return this.searchCourses(keyword, academyId, styleId);
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, params = "search")
	public ModelAndView searchGet(final String keyword, final Integer academyId, final Integer styleId) {
		return this.searchCourses(keyword, academyId, styleId);
	}

	private ModelAndView searchCourses(final String keyword, final Integer academyId, final Integer styleId) {
		ModelAndView result;
		Collection<Course> res;

		res = this.courseService.searchCourses(keyword, academyId, styleId);
		result = new ModelAndView("course/list");

		result.addObject("courses", res);
		if (academyId != null)
			result.addObject("academyId", academyId);
		else if (styleId != null)
			result.addObject("styleId", styleId);

		result.addObject("requestURI", "course/search.do");

		return result;
	}

	@RequestMapping(value = "/listByStyle", method = RequestMethod.GET)
	public ModelAndView listByStyle(@RequestParam final int styleId) {
		ModelAndView result;
		final Style style = this.styleService.findOne(styleId);

		final Collection<Course> courses = style.getCourses();

		result = new ModelAndView("course/list");
		result.addObject("courses", courses);
		result.addObject("styleId", styleId);
		result.addObject("requestURI", "course/listByStyle.do");

		return result;
	}
}
