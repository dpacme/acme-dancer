package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Course;
import domain.Style;
import services.CourseService;
import services.StyleService;

@Controller
@RequestMapping("/style")
public class StyleController extends AbstractController {

	// Services

	@Autowired
	private CourseService	courseService;
	@Autowired
	private StyleService	styleService;


	// Constructors -----------------------------------------------------------

	public StyleController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay(@RequestParam int courseId) {
		ModelAndView result;
		Course course = this.courseService.findOne(courseId);

		result = new ModelAndView("style/showDisplay");
		result.addObject("style", course.getStyle());
		result.addObject("requestURI", "style/showDisplay.do");

		return result;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView myCourses() {
		ModelAndView result;

		Collection<Style> styles = this.styleService.findAll();

		result = new ModelAndView("style/list");
		result.addObject("styles", styles);
		result.addObject("requestURI", "style/list.do");

		return result;
	}

}
