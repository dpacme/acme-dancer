
package controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Apply;
import domain.Course;
import domain.Dancer;
import services.ApplyService;
import services.CourseService;
import services.DancerService;

@Controller
@RequestMapping("/apply")
public class ApplyController extends AbstractController {

	// Services
	@Autowired
	private DancerService	dancerService;

	@Autowired
	private CourseService	courseService;

	@Autowired
	private ApplyService	applyService;


	// Constructors -----------------------------------------------------------

	public ApplyController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	@RequestMapping(value = "/myApplies", method = RequestMethod.GET)
	public ModelAndView myApplies() {
		ModelAndView result;
		Dancer dancer = dancerService.findByPrincipal();

		result = new ModelAndView("apply/list");
		result.addObject("applies", dancer.getApplies());
		result.addObject("requestURI", "apply/myApplies.do");

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam int courseId) {
		ModelAndView result;
		
		try {
			final Dancer dancer = dancerService.findByPrincipal();
			final Course course = courseService.findOne(courseId);
			Collection<Apply> applies = dancer.getApplies();
			for(Apply a:applies) {
				Assert.isTrue(!a.getCourse().equals(course));
			}
			Apply apply = this.applyService.create();
			apply.setCourse(course);
			apply.setDancer(dancer);
			apply.setStatus("pending");
			Date date = new Date();
			apply.setMomment(date);
			this.applyService.save(apply);

			result = new ModelAndView("apply/list");
			result.addObject("applies", dancer.getApplies());
			result.addObject("requestURI", "apply/myApplies.do");
			
		} catch (final Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}
		
		

		return result;
	}

}
