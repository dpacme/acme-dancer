
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Style;

@Component
@Transactional
public class StyleToStringConverter implements Converter<Style, String> {

	@Override
	public String convert(final Style style) {
		String res;

		if (style == null)
			res = null;
		else
			res = String.valueOf(style.getId());

		return res;

	}
}
