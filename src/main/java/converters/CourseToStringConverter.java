
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Course;

@Component
@Transactional
public class CourseToStringConverter implements Converter<Course, String> {

	@Override
	public String convert(final Course course) {
		String res;

		if (course == null)
			res = null;
		else
			res = String.valueOf(course.getId());

		return res;

	}
}
