<%--
 * action-1.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<security:authorize access="hasRole('ADMINISTRATOR')">

	<h2>
		<spring:message code="administrator.minMaxAvgAndStadevOfAppliesPerCourse" />
	</h2>
	<jstl:out value="${minAppliesPerCourse}" />
	<br>
	<jstl:out value="${maxAppliesPerCourse}" />
	<br>
	<jstl:out value="${avgAppliesPerCourse}" />
	<br>
	<jstl:out value="${stdevAppliesPerCourse}" />
	<br>

	<h2>
		<spring:message code="administrator.minMaxAvgAndStadevOfCoursesPerAcademy" />
	</h2>
	
	<jstl:out value="${minCoursesPerAcademy}" />
	<br>
	<jstl:out value="${maxCoursesPerAcademy}" />
	<br>
	<jstl:out value="${avgCoursesPerAcademy}" />
	<br>
	<jstl:out value="${stdevCoursesPerAcademy}" />
	<br>
	
</security:authorize>