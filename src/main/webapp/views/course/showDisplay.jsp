<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div>
	<ul>
		<li><b><spring:message code="course.title" />:</b>
			${course.title}</li>
		<li><b><spring:message code="course.level" />:</b>
			${course.level}</li>
		<li><b><spring:message code="course.starDate" />:</b>
			<fmt:formatDate value="${starDate}" pattern="dd/MM/YYYY" /></li>
		<li><b><spring:message code="course.endDate" />:</b>
			<fmt:formatDate value="${endDate}" pattern="dd/MM/YYYY" /></li>
		<li><b><spring:message code="course.dayWeek" />:</b>
			${course.dayWeek}</li>
		<li><b><spring:message code="course.time" />:</b><fmt:formatDate value="${time}" pattern="HH:mm" /></li>
	</ul>
</div>


<br />
