<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>



<form:form action="${requestURI}" modelAttribute="style">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="courses" />

	<acme:textbox code="style.name" path="name" />
	<acme:textbox code="style.description" path="description" />
	
	<form:label path="pictures">
        <spring:message code="style.pictures" />
    </form:label><b></b>
    <form:input type="url" path="pictures" />
    <form:errors class="error" path="pictures" />	
	<br>
	
	<form:label path="videos">
        <spring:message code="style.videos" />
    </form:label><b></b>
    <form:input type="url" path="videos" />
    <form:errors class="error" path="videos" />	
	<br>
	
	<br>
	<b><spring:message code="style.picturesW" /></b>
	<br>
	<br>
	
	<jstl:if test="${deleteError!=null}">
		<span class="message"><spring:message code="${deleteError}" /></span>
	</jstl:if>
	<br />
	
	<!-- Acciones -->
	<acme:submit name="save" code="style.save"/>
	
	<jstl:if test="${requestURI.contains('edit')}">
		<acme:submit name="delete" code="style.delete"/>
	</jstl:if>
	
	<acme:cancel url="style/list.do" code="style.cancel"/>

</form:form>

<br>

