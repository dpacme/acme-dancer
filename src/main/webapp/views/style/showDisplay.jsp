<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div>
	<ul>
		<li><b><spring:message code="style.name" />:</b>
			${style.name}</li>
		<li><b><spring:message code="style.description" />:</b>
			${style.description}</li>
		<li><b><spring:message code="style.pictures" />:</b>
			<jstl:forEach var="picture" items="${style.pictures}">
				<a href="${picture}" target="_blank"><img src="${picture}"
					height="64" width="64"></a>
			</jstl:forEach>
		</li>
		<li><b><spring:message code="style.videos" />:</b>
			<jstl:forEach var="video" items="${style.videos}">
				<a href="${video.toString()}" target="_blank">${video}</a>
				<br/>
			</jstl:forEach>
		</li>
	</ul>
</div>


<br />
